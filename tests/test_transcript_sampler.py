""" Tests to Run. """
import pytest
from code.transcript_sampler import TranscriptSampler

trS = TranscriptSampler()

@pytest.mark.parametrize(
        "test_input,expected",
        [(("./data/test_gene_data.txt",{"A": 1, "B": 2}))]
)

def test_read(test_input, expected):
    # tests for the read_avg_expression() function

    assert TranscriptSampler.read_avg_expression(test_input) == expected
    #with pytest.raises(ValueError):
    #    trS.read_avg_expression(123)  #tests that an error is raised if we don't pass a string file name
    #with pytest.raises(IOError):
    #    trS.read_avg_expression("file_does_not_exist.csv") #tests that an error is raised if we file does not exist


@pytest.mark.parametrize(
    "test_input,expected",
    [(({"A": 1, "B": 2, "C": 3}, 1), 1)]

)

def test_sample():
    #with pytest.raises(TypeError):
    #    tmp_dict = {}
    #    trS.sample_transcripts(tmp_dict, "acd")  # tests that only an integer can be passed as an argument
    #with pytest.raises(IndexError):
    #    tmp_dict = {}
    #   trS.sample_transcripts(tmp_dict, -10)  # tests that error will be thrown if out of range
    tot = 0
    dic = TranscriptSampler.sample_transcripts(test_input[0], test_input[1])
    for key in dic.keys():
        tot += dic[key]
    assert (tot == expected)

@pytest.mark.parametrize(
        "test_input,expected",
        [(("./data/sample_data2.txt",{"A": 1, "B": 2}))]
)

def test_write():
    #with pytest.raises(OSError):
    #    tmp_dict = {}
    #    trS.write_sample(123, tmp_dict)  # tests that an error is raised if file can't be written
    assert TranscriptSampler.write_sample(test_input) == expected

