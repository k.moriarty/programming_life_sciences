# Kathleen Moriarty
# Programming for Life Sciences
"""Transcript Sampler.

Our project will be to write the code that generates
plausible single cell sequencing data,
a non-trivial task, which will allow us to learn many
aspects of software developing in Python.
To get started, we will write a few simple functions,
specifically to generate a stochastic
sample of transcribed copies of a gene, given the average
expression level of genes in cells of a given type.
The functions should be as follows:

    read_avg_expression(file)
        reads a file in the format "gene_id nr_copies"
        constructs a dictionary with the average number of
        transcripts for every gene (i.e. nr_copies)
        return this dictionary

    sample_transcripts(avgs, number)
        takes as input the dictionary constructed above and a
        total number of transcripts to sample
        it generates a sample of transcripts by sampling from
        individual genes in accordance to the
        relative abundance computed from the input dictionary
        returns the dictionary built from this sample

    write_sample(file, sample)
        takes as input a file name and a sample dictionary
        (constructed as described at point 2)
        writes to the file the sample dictionary
"""


class TranscriptSampler:
    """TranscriptSampler Class.

    A class for TranscriptSampler
    """

    def read_avg_expression(self, gene_file):
        """Reads gene file and creates a transcript sampler dictionary.

        Reads a file in the format "gene_id nr_copies" and constructs a
        dictionary with the average number of transcripts for
        every gene provided in gene_file.

        Constructs a dictionary with the average number
        of transcripts for every gene (i.e. nr_copies)

        Args:
            gene_file (str): a file of genes in the
            format of gene_id, nr_copies
        Returns:
            list: a list of the values of the mean transcripts
            for the genes listed in the gene_list
        """
        import pandas as pd

        # Check to make sure file exists
        try:
            f = open(gene_file)
            f.close()
        except IOError:
            print("File not found.")

        # Read in file as dataframe
        gene_df = pd.read_csv(gene_file, header=0)

        # Take the average of the transcripts, grouping by gene
        gene_mean_df = gene_df.groupby('gene_id').mean()
        # print(gene_mean_df.iloc[0:11,])

        # Convert to dictionary
        gene_mean_dict = gene_mean_df.to_dict()

        return(list(gene_mean_dict.values())[0])
        # return (gene_mean_dict)

    def sample_transcripts(self, gene_dict, samp_nr):
        """Sampling gene transcripts.

        Takes as input the dictionary constructed from read_avg_expression and
        a total number of transcripts to sample.
        Then generates a sample of transcripts by sampling from individual
        genes in accordance to the relative abundance
        computed from the input dictionary

        Args:
            gene_dict (dict): Gene dictionary with relative frequency
            samp_nr (int): Number of samples

        Returns:
            dict: A dictionary of samples of genes

        """
        import random

        # To store the values from the sampling
        # Create probabilities for each gene in the gene_dict
        # print((gene_dict).values())
        total_trans = sum(gene_dict.values())
        # print(total_trans)
        prob_dist = {k: v / total_trans for k, v in gene_dict.items()}
        # prob_dist = (gene_dict.values())/total_trans
        # print(prob_dist)

        # Check to confirm that samp_nr is within the range of samples
        try:
            sample_dict = random.choices(population=list(gene_dict.items()),
                                         k=samp_nr,
                                         weights=list(prob_dist.values()))
            return (dict(sample_dict))

        except IndexError:
            print('Sample number larger than gene list length')
            raise
        except TypeError:
            print('Please enter an integer value')
            raise

    def write_sample(self, myfile, tmp_samp_dict):
        """Writes dict to file.

        Takes as input a file name and a sample
        dictionary and saves the dictionary to a file.

        Args:
            myfile (str): File name where to save the dictionary
            tmp_samp_dict (dict): The dictionary to save to file

        Returns:
            nothing

        """
        # Save file
        with open(myfile, 'w') as myfile:
            myfile.write(str(tmp_samp_dict))

        return
