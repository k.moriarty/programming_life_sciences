"""Command Line Processing File.

Interprets the arguments from the commandline,
creates instance of TranscriptSampler and
runs the functions from TranscriptSampler class

"""

import argparse
from transcript_sampler import TranscriptSampler

parser = argparse.ArgumentParser(description='Sample Transcripts')
parser.add_argument('--sample_number', type=int, help='number to sample')
parser.add_argument('--gene_file', type=str, help='source file name')
parser.add_argument('--my_file', type=str, help='dest file name')

def main():
    """Main function.

    Interprets the arguments from the commandline,
    creates instance of TranscriptSampler and
    runs the functions from TranscriptSampler class.
    """

    args = parser.parse_args()
    transcript_obj = TranscriptSampler()
    samp_dict = transcript_obj.read_avg_expression(args.gene_file)
    new_dict = transcript_obj.sample_transcripts(samp_dict, args.sample_number)
    transcript_obj.write_sample(args.my_file, new_dict)


if __name__ == '__main__':
    """Initiates main.
    Calls main function only if run from commandline, but not if imported.
    """
    main()
