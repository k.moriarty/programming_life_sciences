"""Setup Packages.

All of the set up required
"""

from setuptools import setup, find_packages

setup(
    name='transcript_sampling',
    url='https://gitlab.com/k.moriarty/Life_Science_Course.git',
    author='Kathleen Moriarty',
    author_email='kathleen.moriarty@swisstph.ch',
    description='Transcription Sampler',
    license='MIT',
    version='0.1.0',
    packages=find_packages(include=['code/']),
    # this will autodetect Python packages from the directory tree
    # entry_points=['my-executable=code.cli:main'],
    entry_points = {
        'console_scripts': ['my-executable=code.cli:main'],
    },
    install_requires=['pandas', 'argparse']
    # packages that are required for your package to run inc. version or range
)
